package week5.day3;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegularExpression {

	public static void main(String[] args) {
	
		
		//7. Validate Creditcard number
		String cc="1111 2222 3333 5555";
		
		//String pattern="[0-9]{4}\b[0-9]{4}\b[0-9]{4}\b[0-9]{4}";
		String pattern="\\d{4}\\s\\d{4}\\s\\d{4}\\s\\d{4}";
		
		
		Pattern compile=Pattern.compile(pattern);
		Matcher match=compile.matcher(cc);
		System.out.println(match.matches());
		
		
		String email="vijayalaxmi.thilaga@gmail.com";
		
		//String pattern="[0-9]{4}\b[0-9]{4}\b[0-9]{4}\b[0-9]{4}";
		String pattern1="[a-z]{20,20}\\W+";
		
		Pattern compile1=Pattern.compile(pattern1);
		Matcher match1=compile.matcher(email);
		System.out.println(match1.matches());

	}

}
