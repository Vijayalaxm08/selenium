package week5.day3;

import org.apache.commons.math3.geometry.spherical.oned.ArcsSet.Split;

public class InputOutput {

	public static void main(String[] args) {
		
		//Method 1
		String a="axdxcdgxbxx";
		String replace=a.replace("x", "");
			
		System.out.println(replace);
		
		char[] ch=replace.toCharArray();
		StringBuilder sb=new StringBuilder(replace);
		for (int i = 0; i < ch.length-1; i++) {
			sb.append("x");
		}
		
		System.out.println(sb);
		
		
		//Method 2
		String a1="axdxcdgxbxx";
		String newInput="";
		char[] charArray=a1.toCharArray();
		for (char c : charArray) {
			if(c=='x') {
				newInput+=c;
			}
		}
		String  replace1=a1.replace("x", "");
		String concat=replace1.concat(newInput);
		System.out.println(concat);
	

	}

}
