package week5.day2;

public class StringMethod {

	public static void main(String[] args) {

		//Reverse a String
		StringBuffer name=new StringBuffer("Vijayalaxmi");
		System.out.println(name.reverse());
		
		//Another Method
		String text="Testleaf";
		System.out.println(text.length());
		
		for (int i = text.length()-1; i>=0; i--) {
			char charAt = text.charAt(i);
			System.out.print(charAt);
		}
		
		//Another Method
		String text1="Testleaf";
		char[] charArray = text1.toCharArray();
								
		for (int i = charArray.length-1; i>=0; i--) {
			System.out.println(charArray[i]);
		}
		
				
		//2. Print the characters between 1st s and last s
		String a="hi pls find the in between characters";
		System.out.println("  ");
		System.out.println(a.length());
		int startString = a.indexOf("s");
		int lastString=a.lastIndexOf("s");
		
		System.out.println(startString);
		System.out.println(lastString);
		
		String substring = a.substring(startString+1, lastString);
		System.out.println(substring);
		System.out.println(substring.length());
		
		
		//3.Count the Number of 'a' in the string
		
		String var="Amazon India";
		char[] eachChar=var.toCharArray();
		int count=0;
		for (char c : eachChar) {
			if (c=='A'||c=='a') {
				count++;
			}
		}
		System.out.println(count);	
	}
	
	//4. Take a name list in string array print if it starts with 'a' or contains 'b'
	String[] listOfName= {"Abi", "Babu", "Ch","Anu"};
	
	
			

}


/*
Selenium HW:

1. Goto https://erail.in/
2. Enter source as MAS
3. Enter destination as SBC
4. Store all the train names and sort in ascending order
5. Click on the Train Name link in the header of table
(It will do the sorting automatically)
6. Verify the sorted and the values you stored are same.*/
