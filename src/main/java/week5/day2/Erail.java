package week5.day2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Erail {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
	//1. Goto https://erail.in/
		driver.get("https://erail.in/");
		driver.manage().window().maximize();
	 
//		2. Enter source as MAS
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
		
//		3. Enter destination as SBC
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("SBC", Keys.TAB);
		driver.findElementById("buttonFromTo").click();
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("chkSelectDateOnly").click();
		
//		4. Store all the train names and sort in ascending order
		WebElement table;
		List<WebElement> rows;
		table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		//Find Rows
		rows = table.findElements(By.tagName("tr"));
		String trainNames = null;
		List<String> trains=new ArrayList<>();
		for (int i = 0; i < rows.size(); i++) {
			
			List<WebElement> cols = rows.get(i).findElements(By.tagName("td"));
			trainNames = cols.get(1).getText();
					
			trains.add(trainNames);
			
			Collections.sort(trains);
								
		}
		
		/*for (String sortedTrainList : trains) {
			
			System.out.println(sortedTrainList);
		}*/
		
//		5. Click on the Train Name link in the header of table
		driver.findElementByLinkText("Train Name").click();
		
//		(It will do the sorting automatically)
//		6. Verify the sorted and the values you stored are same.
		table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		//Find Rows
		rows = table.findElements(By.tagName("tr"));
		String sorttrainNames = null;
		
		List<String> sortedTrains=new ArrayList<>();
       for (int i = 0; i < rows.size(); i++) {
			
			List<WebElement> cols = rows.get(i).findElements(By.tagName("td"));
			sorttrainNames = cols.get(1).getText();
					
			sortedTrains.add(sorttrainNames);
				
											
		}
		
              
		for (String sortedTrainList : trains) {
			
			  for (String trainList : sortedTrains) {
										
					if (sortedTrainList.equals(trainList)) {
						
						System.out.println("The Sorted List : " +sortedTrainList+
								" equals the trains list manually sorted "+trainList);
					}
				   
			       }
			
		}
	}

}