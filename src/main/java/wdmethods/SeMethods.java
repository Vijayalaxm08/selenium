package wdmethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import utils.Report;

public class SeMethods extends Report implements WdMethods {
	
	RemoteWebDriver driver=null;
	@Override
	public void startApp(String browser, String url) {
		// TODO Auto-generated method stub
		try {
			if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("driver.chrome.driver", "./drivers/chromedriver.exe");
				driver=new ChromeDriver();
			}else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("driver.gecko.driver", "./drivers/geckodriver.exe");
				driver=new FirefoxDriver();
			}else if(browser.equalsIgnoreCase("ie")) {
				System.setProperty("driver.ie.driver", "./drivers/IEDriverServer.exe");
				driver=new InternetExplorerDriver();
			}		
			driver.get(url);
			driver.manage().window().maximize();
			System.out.println("The Browser " +browser+ " is launched successfully");
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		} catch (WebDriverException e) {
			
			System.err.println("Web Driver Exception - Driver Not Launched Successfully");
			
		}
}

	//@SuppressWarnings("finally")
	@Override
	public WebElement locateElement(String locator, String locValue) {
		
		try {
			switch (locator) {
			case "id":
				return driver.findElementById(locValue);
			case "name":
				return driver.findElementByName(locValue);
			case "class":
				return driver.findElementByClassName(locValue);
			case "link":
				return driver.findElementByLinkText(locValue);
			case "tag":
				return driver.findElementByTagName(locValue);
			case "xpath":
				return driver.findElementByXPath(locValue);
			}
		} catch (NoSuchElementException e) {
			
			System.err.println("Element is Not Found: NoSuchElementException");
			
		} catch(WebDriverException e1) {
			
			System.err.println("Driver is Not Found: WebDriverException");
		} 	
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void type(WebElement ele, String data) {
	   try {
		ele.clear();
		   ele.sendKeys(data);
		   System.out.println("The Data " +data+ " is entered successfully");
		} catch (NoSuchElementException e) {
			
			System.err.println("Element is Not Found: NoSuchElementException");
		}
	}

	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			System.out.println("The element "+ele+" clicked successfully");
		} catch (NoSuchElementException e) {
			System.err.println("Element is Not Found: NoSuchElementException");
		}
		
	}

	@Override
	public String getText(WebElement ele) {
		String text = null;
		try {
			text = ele.getText();
			System.out.println("The Text is Displayed as: " +text);
		} catch (NoSuchElementException e) {
			System.err.println("Element is Not Found: NoSuchElementException");			
		}
		return text;		
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select s=new Select(ele);
			s.selectByVisibleText(value);		
			System.out.println("The value "+value+" entered successfully");
		} catch (NoSuchElementException e) {
			System.err.println("Element is Not Found: NoSuchElementException");			
		}
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			Select s=new Select(ele);
			s.selectByIndex(index);
		} catch (NoSuchElementException e) {
			System.err.println("Element is Not Found: NoSuchElementException");			
		}
	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		String browserTitle = driver.getTitle();
		if(browserTitle.equals(expectedTitle)) {
			System.out.println("The title of the browser is" +browserTitle);
			return true;
			
		}else {
			System.out.println("The title of the browser is" +browserTitle);
			return false;
		}
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		try {
			String text = ele.getText();
			if(text.equalsIgnoreCase(expectedText)) {
				System.out.println("The Text is Displayed Correctly as: " +text);
			}else {
				System.out.println("The Text is Not Displayed Correctly as"+text);
			}
		} catch (NoSuchElementException e) {
			System.err.println("Element is Not Found: NoSuchElementException");			
		}
		
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		try {
			String text = ele.getText();
			if(text.contains(expectedText)) {
				System.out.println("The Text is Displayed Correctly as: " +text);
			}else {
				System.out.println("The Text is Not Displayed Correctly as"+text);
			}
		} catch (NoSuchElementException e) {
			System.err.println("Element is Not Found: NoSuchElementException");			
		}
		
	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		try {
			String attribute1 = ele.getAttribute(attribute);
			if(attribute1.equalsIgnoreCase(value)) {
				System.out.println("The "+attribute+" has the value "+attribute1);
			}else {
				System.out.println("The attribute is not matched");
			}
		} catch (NoSuchElementException e) {
			System.err.println("Element is Not Found: NoSuchElementException");			
		}	
		
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		try {
			String attribute1 = ele.getAttribute(attribute);
			if(attribute1.contains(value)) {
				System.out.println("The "+attribute+" has the value "+attribute1);
			}else {
				System.out.println("The attribute is not matched");
			}
		} catch (NoSuchElementException e) {
			System.err.println("Element is Not Found: NoSuchElementException");			
		}
						
	}

	@Override
	public void verifySelected(WebElement ele) {
		try {
			if (ele.isSelected()) {
				System.out.println("The Element is Displayed");
			}else {
				System.out.println("The Element is Not Displayed");
			}
		} catch (NoSuchElementException e) {
			System.err.println("Element is Not Found: NoSuchElementException");			
		}
		
	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		try {
			if (ele.isDisplayed()) {
				System.out.println("The Element is Displayed");
			}else {
				System.out.println("The Element is Not Displayed");
			}
		} catch (NoSuchElementException e) {
			System.err.println("Element is Not Found: NoSuchElementException");			
		}
		
	}

	@Override
	public void switchToWindow(int index) {
		
		try {
			Set<String> set=driver.getWindowHandles();
			
			List<String> list=new ArrayList<>();
			list.addAll(set);
			driver.switchTo().window(list.get(index));
			System.out.println("The title of Newly Switched Window" +driver.getTitle());
		} catch (NoSuchWindowException e) {
			System.err.println("Element is Not Found: NoSuchWindowException");			
		}				
	}

	@Override
	public void switchToFrame(WebElement ele) {
		
		try {
			driver.switchTo().frame(ele);
			System.out.println("Switched To Frame");
		} catch (NoSuchFrameException e) {
			System.err.println("Element is Not Found: NoSuchFrameException");			
		}
	}

	@Override
	public void acceptAlert() {
		try {
			driver.switchTo().alert().accept();
			System.out.println("Alert is accepted");
		} catch (UnhandledAlertException e) {
			System.err.println("Element is Not Found: UnhandledAlertException");			
		} catch(NoAlertPresentException e) {
			System.err.println("Element is Not Found: NoAlertPresentException");	
		}
	}

	@Override
	public void dismissAlert() {
		try {
			driver.switchTo().alert().dismiss();
			System.out.println("Alert is accepted");
		} catch (UnhandledAlertException e) {
			System.err.println("Element is Not Found: UnhandledAlertException");			
		} catch(NoAlertPresentException e) {
			System.err.println("Element is Not Found: NoAlertPresentException");	
		}	
	}

	@Override
	public String getAlertText() {
		String alertText = null;
		try {
			alertText = driver.switchTo().alert().getText();
			System.out.println("The Text of the alert is " +alertText);
		} catch (UnhandledAlertException e) {
			System.err.println("Element is Not Found: UnhandledAlertException");			
		} catch(NoAlertPresentException e) {
			System.err.println("Element is Not Found: NoAlertPresentException");	
		}	
		return alertText;		
	}

	@Override
	public void takeSnap() throws IOException {
		
		try {
			File src = driver.getScreenshotAs(OutputType.FILE);
			File dest=new File("./screenshots/CreateLead.png");
			
			FileUtils.copyFile(src, dest);
		} catch (WebDriverException e) {
			System.err.println("Element is Not Found: NoSuchWindowException");	
		}
		
	}

	@Override
	public void closeBrowser() {
		try {
			driver.close();
			System.out.println("The Current Browser is Closed");
		} catch (WebDriverException e) {
			System.err.println("Element is Not Found: NoSuchWindowException");	
		}
		
	}

	@Override
	public void closeAllBrowsers() {
		try {
			driver.quit();
			System.out.println("All the Browser is Closed");
		} catch (WebDriverException e) {
			System.err.println("Element is Not Found: NoSuchWindowException");	
		}
		
	}

}
