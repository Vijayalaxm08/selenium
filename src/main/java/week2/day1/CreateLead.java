package week2.day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class CreateLead {

	public void main() {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		//launch URL
		driver.get("http://leaftaps.com/opentaps");
		//Maximize the Browser window
		driver.manage().window().maximize();
		
		//Enter UserName
		driver.findElementById("username").sendKeys("DemoSalesManager");
		//Enter Password
		driver.findElementById("password").sendKeys("crmsfa");
		//Click Login
		driver.findElementByClassName("decorativeSubmit").click();
		
		//Click on Link 
		driver.findElementByLinkText("CRM/SFA").click();
		//Click on Leads
		driver.findElementByLinkText("Leads").click();
		//Click on Create Lead
		driver.findElementByLinkText("Create Lead").click();
		
		//Enter Company Name
		driver.findElementById("createLeadForm_companyName").sendKeys("QERWER");
		//Enter First Name
		driver.findElementById("createLeadForm_firstName").sendKeys("Vijayalaxmi");
		//Enter Last Name
		driver.findElementById("createLeadForm_lastName").sendKeys("Krishnarajan");	
		
		//Entering All the available Fields
		driver.findElementById("createLeadForm_dataSourceId").sendKeys("Public Relations");
		driver.findElementById("createLeadForm_marketingCampaignId").sendKeys("eCommerce Site Internal Campaign");
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Vijayalaxmi Local");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Krishnarajan Local");	
		driver.findElementById("createLeadForm_personalTitle").sendKeys("Ms");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Ms");
		driver.findElementById("createLeadForm_departmentName").sendKeys("Automation");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("1200000");
		driver.findElementById("createLeadForm_currencyUomId").sendKeys("INR - Indian Rupee");
		driver.findElementById("createLeadForm_industryEnumId").sendKeys("IND_SOFTWARE");
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("50");
		driver.findElementById("createLeadForm_ownershipEnumId").sendKeys("Partnership");
		driver.findElementById("createLeadForm_sicCode").sendKeys("772577");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("Yes");
		driver.findElementById("createLeadForm_description").sendKeys("Creation of Create Lead");
		driver.findElementById("createLeadForm_importantNote").sendKeys("Important Note");
		
		//Contact Information
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("91");
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9003228239");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("66016");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Krishnarajan");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("vijayalaxmi.thilaga@gmail.com");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("https://www.gmail.com");
		
		//Primary Address
		driver.findElementById("createLeadForm_generalToName").sendKeys("Viji");
		driver.findElementById("createLeadForm_generalAttnName").sendKeys("Thilaga");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("AGS Villa");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("Pallikaranai");
		driver.findElementById("createLeadForm_generalCity").sendKeys("Chennai");
		driver.findElementById("createLeadForm_generalStateProvinceGeoId").sendKeys("Minnesota");
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("600100");
		driver.findElementById("createLeadForm_generalCountryGeoId").sendKeys("India");
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("001");
		
		//Click Create Lead
				driver.findElementByClassName("smallSubmit").click();
											
		driver.close();

	}

}
