package week2.day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class IFrame {

	public static void main(String[] args) {

		System.setProperty("webdrriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		//Launch the URL
		driver.get("http://layout.jquery-dev.com/demos/iframe_local.html");
		driver.manage().window().maximize();
		
		//IFrame West - Close Me
		driver.switchTo().frame("childIframe");
		driver.findElementByXPath("(//button[text()='Close Me'])[1]").click();
		driver.switchTo().defaultContent();
		System.out.println("IFrame West Closed");
		
		//IFrame East - Close Me
		driver.switchTo().frame("childIframe");
		driver.findElementByXPath("(//button[text()='Close Me'])[2]").click();
		driver.switchTo().defaultContent();
		System.out.println("IFrame East Closed");
		
		//West - Close Me
		driver.findElementByXPath("(//button[text()='Close Me'])[1]").click();
		System.out.println("West Closed");
		
		//East - Close Me
		driver.findElementByXPath("(//button[text()='Close Me'])[2]").click();
		System.out.println("East Closed");
		
//		driver.switchTo().frame("childIframe");
//		driver.findElementByXPath("(//button[text()='Close Me'])[2]").click();
//		driver.switchTo().defaultContent();
				
	}

}
