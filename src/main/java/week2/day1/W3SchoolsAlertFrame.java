package week2.day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class W3SchoolsAlertFrame {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdrriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		//Launch the URL
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().window().maximize();
		
		//Clicking on Try it Button
		//Switching to Frame	
		driver.switchTo().frame("iframeResult");
		
		driver.findElementByXPath("//button[text()='Try it']").click();
		Thread.sleep(4000);
		
		//driver.switchTo().defaultContent();
		
		//Entering any text in alert box and click ok
		driver.switchTo().alert().sendKeys("Vijayalaxmi Krishnarajan");
		String alertText = driver.switchTo().alert().getText();
		System.out.println("The Alert Text Displayed is: " +alertText);
		driver.switchTo().alert().accept();
		
		//Validating the Frame Content
		driver.switchTo().defaultContent();
		driver.switchTo().frame("iframeResult");
		
		String textFrame = driver.findElementById("demo").getText();
		System.out.println(textFrame);
		
		driver.switchTo().defaultContent();
		
		driver.close();		

	}

}
