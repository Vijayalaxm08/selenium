package week2.day3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ListRemoveDuplicates {

	//Removing Duplicates Using Set 
	
	public static void main(String[] args) {
		
		List<String> list=new ArrayList<String>();
		list.add("gopi");
		list.add("sarath");
		list.add("babu");
		list.add("viji");
		list.add("babu");
		list.add("babu");
		list.add("babu");
		
		System.out.println(list);
		Set<String> set = new HashSet<>();
		set.addAll(list);
		System.out.println(set);
		/*System.out.println("The Size of the List is :" +list.size());
		for (String eachList : list) {
			
			System.out.println("The List contains the value :" +eachList);
					
		}
		
		list.remove("babu");
		System.out.println("The Size of the List after removing value:" +list.size());
		
		list.add("Karthik");
		list.remove(2);
		System.out.println("The Size of the List is after removing using index :" +list.size());*/
	}

}
