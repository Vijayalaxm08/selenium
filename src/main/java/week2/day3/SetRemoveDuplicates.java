package week2.day3;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetRemoveDuplicates {

	public static void main(String[] args) {
	
		//Hash Set
		Set<String> set=new HashSet<String>();
		set.add("Karthik");
		set.add("gopi");
		set.add("babu");
		set.add("sarath");
		set.add("Karthik");
		
		System.out.println("The Size of the Set is "+ set.size());
		
		for (String eachSet : set) {
			System.out.println("The Set contains the values: " +eachSet);
		}
		
		//Tree Set
		Set<String> set1=new TreeSet<String>();
		set1.add("Karthik");
		set1.add("gopi");
		set1.add("babu");
		set1.add("sarath");
		set1.add("Karthik");
		
		System.out.println("The Size of the Set is "+ set1.size());
		
		for (String eachSet1 : set1) {
			System.out.println("The Set contains the values: " +eachSet1);
		}
		
		//Linked Hash Set
		Set<String> set2=new LinkedHashSet<String>();
		set2.add("Karthik");
		set2.add("gopi");
		set2.add("babu");
		set2.add("sarath");
		set2.add("Karthik");
		
		System.out.println("The Size of the Set is "+ set2.size());
		
		for (String eachSet2 : set2) {
			System.out.println("The Set contains the values: " +eachSet2);
		}
		
	}

}
