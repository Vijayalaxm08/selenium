package week2.day2;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandle {

	public static void main(String[] args) {
	 
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().window().maximize();
			
		String titleSignUp = driver.getTitle();
		System.out.println("The Title of Sign Up Page is: " +titleSignUp);
		
		String currentUrlSignUp = driver.getCurrentUrl();
		System.out.println("The URL of the Sign Up Page is: " +currentUrlSignUp);
		
		driver.findElementByLinkText("Contact Us").click();
		
		//Getting all Widow ID
		Set<String> allWindow = driver.getWindowHandles();
		
		//Converting Set To List 
		List<String> list=new ArrayList<String>();
		list.addAll(allWindow);
		//Switching To Second Window
		driver.switchTo().window(list.get(1));
		
		//Title of New Window
		String titleContactUs = driver.getTitle();
		System.out.println("The Title of Contact Us Page is: " +titleContactUs);
		
		String currentUrlContactUs = driver.getCurrentUrl();
		System.out.println("The URL of the Contact Us Page is: " +currentUrlContactUs);
		
		//Closing First Window
		driver.switchTo().window(list.get(0));
		driver.close();
		System.out.println("SignUp Window is Closed");
		
		//Closing Second Widow
		driver.switchTo().window(list.get(1));
		driver.close();
		System.out.println("Contact Us Window is Closed");
		
		//Closing All Window
		driver=new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.findElementByLinkText("Contact Us").click();
//		driver.quit();
//		System.out.println("All the Windows are Closed");
		driver.findElementByXPath("//font[text()='Counter Ticket Cancellation']").click();
		
		allWindow = driver.getWindowHandles();
		
		List<String> list1=new ArrayList<String>();
		list1.addAll(allWindow);
		
		driver.switchTo().window(list1.get(1));
		System.out.println(driver.getTitle());
		
		driver.switchTo().window(list1.get(2));
		System.out.println(driver.getTitle());
					
		
	}

}
