package week2.day2;

import java.util.ArrayList;
import java.util.List;

public class ListLearn {

	public static void main(String[] args) {
		
		LearnList();
	
	}

	public static void LearnList() {
		List<String> list=new ArrayList<String>();
		list.add("gopi");
		list.add("sarath");
		list.add("babu");
		list.add("gopi");
		list.add("viji");
		
		System.out.println("The Size of the List is : " +list.size());
		
		int count=0;
		for (String eachList : list) {
						
			if(eachList.startsWith("g")) {
				System.out.println(eachList);
				count=count+1;				
			
			}
							
		}
		System.out.println("The Count of List starting with g is: " +count);
	}


}