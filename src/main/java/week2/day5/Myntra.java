package week2.day5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.collect.Collections2;

import io.cucumber.datatable.dependency.com.fasterxml.jackson.databind.ser.std.CollectionSerializer;

public class Myntra {

	public static void main(String[] args) {
		
//		1	Lauch the chrome browser 
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
//		2	Open Myntra website 
		driver.get("https://www.myntra.com/");
		driver.manage().window().maximize();			
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
//		3	In Searchbox type "Sunglasses"
		driver.findElementByClassName("desktop-searchBar").sendKeys("Sunglasses");
		
//		4	Get the List of all products 
		driver.findElementByXPath("//span[contains(@class,'iconSearch')]").click();
		WebDriverWait wait=new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.titleContains("Sunglasses"));
		List<WebElement> list=driver.findElementsByXPath("//div[@class='product-brand']");
		System.out.println("The Total List of All Products is:" +list.size());
		
//		5	get the product with 40% discount
		
	
		
//		6	And the porduct should be Unisex product
//		7	Finally Display the brand name and price of the product
//		8	Click on Face Shape
//		9	Click on round checkbox
//		10	Click on type
//		11	Click on Oval Sunglasses
//		12	Click on the first product 
//		13	Get the product name 
//		14	Add to Bag 
//		15	Go to Bag
//		16	Verfiy the product name 
//		17	close browser


	}

}
