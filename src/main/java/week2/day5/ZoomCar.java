package week2.day5;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ZoomCar {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		driver.get("https://www.zoomcar.com/chennai");
		driver.manage().window().maximize();
		
		driver.findElementByLinkText("Start your wonderful journey").click();
		
		//3	 In the Search page, Click on the any of the pick up point under POPULAR PICK-UP POINTS
		//driver.findElementByXPath("//input[contains(@placeholder,'Tell us your Starting')]").sendKeys(keysToSend);
		driver.findElementByXPath("(//div[@class='items'])[1]").click();
		
		//4	 Click on the Next button
		driver.findElementByXPath("//button[text()='Next']").click();
		
		//5	 Specify the Start Date as tomorrow Date
		// Get the current date
		Date date = new Date();
				
		// Get only the date (and not month, year, time etc)
		DateFormat sdf = new SimpleDateFormat("dd");
		
		// Get today's date
		String today = sdf.format(date);
				
		// Convert to integer and add 1 to it
		int tomorrow = Integer.parseInt(today)+1;
				
		// Print tomorrow's date
		System.out.println("Tomorrow's Date is "+tomorrow);
		
		String xPath = "//div[contains(text(),'"+tomorrow+"')]";
		driver.findElementByXPath(xPath).click();
		
//		6	 Click on the Next Button
		driver.findElementByXPath("//button[text()='Next']").click();
		
//		7	 Confirm the Start Date and Click on the Done button
		int endDate=tomorrow+1;
		String xPath1 = "//div[contains(text(),'"+endDate+"')]";
		String enabled = driver.findElementByXPath(xPath1).getAttribute("class");
		if(enabled.contains("day picked ")) {
			System.out.println("The Start Date is displayed correctly as :"+endDate);
		}else {
			System.out.println("The start date is not displayed correctly");
		}
		driver.findElementByXPath("//button[text()='Done']").click();
		
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		
		//8	 In the result page, capture the number of results displayed
		//9	 Find the highest value and report the brand name
//		10	 click on the Book Now button for it
		List<WebElement> list=driver.findElementsByXPath("//div[@class='car-listing']");
		List<Integer> priceOfCar=new ArrayList<Integer>();
		List<String> brandName=new ArrayList<String>();
		System.out.println("The Total Result of CARS displayed is "+list.size());
		for (WebElement eachCar : list) {
			String price = eachCar.findElement(By.className("price")).getText();
			//priceOfCar.add(Integer.parseInt(price.replaceAll("₹","")));
			priceOfCar.add(Integer.parseInt(price.replaceAll("₹ ","")));
			//System.out.println(priceOfCar);
			
			String brand = eachCar.findElement(By.tagName("h3")).getText();
			brandName.add(brand);			
		}
		
		int max = Collections.max(priceOfCar);
		System.out.println(max);
		for (int i = 0	; i < list.size(); i++) {
			if(max==(priceOfCar.get(i))) {
				System.out.println(brandName.get(i));
				System.out.println(i);
				driver.findElementByXPath("(//button[text()='BOOK NOW'])["+(i+1)+"]").click();
			}
		
		}
			
		System.out.println("The Cab is Booked Successfully");
		
//		11	 Close the Browser
		driver.close();

	}

}
