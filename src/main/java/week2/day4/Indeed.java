package week2.day4;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Indeed {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		driver.get("https://www.indeed.co.in/Fresher-jobs");
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			       
	    List<WebElement> list=driver.findElementsByXPath("//a[@data-tn-element='jobTitle']");
	    System.out.println(list.size());
	    
	    for (WebElement eleEach : list) {
	    	
	    	System.out.println(eleEach.getText());
	    	
	    	Actions builder=new Actions(driver);
	    	builder.sendKeys(eleEach,Keys.CONTROL).pause(10).click(eleEach).perform();
	    	Set<String> window = driver.getWindowHandles();
	    	
	    	List<String> list1=new ArrayList<String>();
	    	list1.addAll(window);
	    	
	    	driver.switchTo().window(list1.get(1));
	    	String titleNewWindow = driver.getTitle();
	    	System.out.println("The Title of the Page is :"+titleNewWindow);
	        //driver.close();
	        driver.switchTo().window(list1.get(0));
	        WebDriverWait wait1=new WebDriverWait(driver, 30);
	    	wait1.until(ExpectedConditions.titleContains("Fresher"));
	    				
		}
	    
	    driver.quit();
    		
	}

}
