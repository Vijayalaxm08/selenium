package week2.day4;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Draggable {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		driver.get("http://jqueryui.com/draggable/");
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.switchTo().frame(0);
		WebElement drag = driver.findElementById("draggable");
		int x = driver.findElementById("draggable").getLocation().getX();
		int y = driver.findElementById("draggable").getLocation().getY();
		Actions builder=new Actions(driver);
		builder.clickAndHold(drag).moveByOffset(x+100, y+100).perform();
			
	}

}
