package week2.day4;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Sortable {

	public static void main(String[] args) {
	
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		driver.get("http://jqueryui.com/sortable/");
		driver.manage().window().maximize();
		
		driver.switchTo().frame(0);
		Actions builder = new Actions(driver);
		WebElement item1 = driver.findElementByXPath("//li[text()='Item 1']");
		WebElement item5 = driver.findElementByXPath("//li[text()='Item 5']");
		int x = driver.findElementByXPath("//li[text()='Item 5']").getLocation().getX();
		int y = driver.findElementByXPath("//li[text()='Item 5']").getLocation().getY();
		builder.dragAndDropBy(item1, x, y).perform();
	}

}
