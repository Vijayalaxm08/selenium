package week4.day4;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.internal.runners.model.EachTestNotifier;

public class LearnMap {

	public static void main(String[] args) {

		String text="testleaf";
		char[] ch=text.toCharArray();
		System.out.println("The Characters in the text is: "+ch.length);
				
		Map<Character,Integer> map=new HashMap<Character,Integer>();
		
		for (char c : ch) {
			
			System.out.println(c);
			
			if(map.containsKey(c)) {
				map.put(c, map.get(c)+1);
			}else {
				map.put(c, 1);
			}
			
		}		
		
		System.out.println(map);	
	}

}
