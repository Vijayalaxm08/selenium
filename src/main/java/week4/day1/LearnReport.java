package week4.day1;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReport {

	public static void main(String[] args) {
		
		//Path of the Report
		ExtentHtmlReporter html=new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);
		
		ExtentReports extent=new ExtentReports();
		
		//Attach Report
		extent.attachReporter(html);
		ExtentTest test=extent.createTest("TC001_Login", "Leaftap Login");
		test.assignAuthor("Viji");
		test.assignCategory("Smoke");
		test.pass("Enter username successfully");
		test.pass("Enter password successfully");
		test.pass("Click login successful");
		test.fail("Login not successful");
		
		//Generate Report
		extent.flush();
	}

}
