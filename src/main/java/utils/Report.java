package utils;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Report {
		
	ExtentHtmlReporter html;
	ExtentReports extent;
	ExtentTest test;
	public String testName, testDesc, author, category;
	
		//Path of HTML Report
		@BeforeSuite
		public void createReport() {
			html=new ExtentHtmlReporter("./reports/Results.html");
			html.setAppendExisting(true);
			extent=new ExtentReports();
			extent.attachReporter(html);
		}
		
		//Attach Report
		@BeforeClass
		public void createTest() {
			test=extent.createTest(testName, testDesc);
			test.assignAuthor(author);
			test.assignCategory(category);
		}
			
		
		public void reportStep(String status, String data) {
			
			if (status.equalsIgnoreCase("Pass")) {
				test.pass(data);
			}else if (status.equalsIgnoreCase("Fail")) {
				test.fail(data);				
			}else if (status.equalsIgnoreCase("Warning")) {
				test.warning(data);			
			}
		}
		
		//Generate Report
		@AfterSuite
		public void closeReport() {
			extent.flush();
		}
		


}
