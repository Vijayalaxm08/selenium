package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

public class ReadExcelData {
	  
	  public static String[][] readExcelData(String fileName) throws IOException {
			
			//1. Open Workbook
			XSSFWorkbook wbook=new XSSFWorkbook("./data/"+fileName+".xlsx");
						
			//2. Go to Sheet
			XSSFSheet sheet=wbook.getSheetAt(0);
			
			//Row Count
			int rowCount=sheet.getLastRowNum();
			
			//Column Count
			int columnCount=sheet.getRow(0).getLastCellNum();
			
			String[][] data=new String[rowCount][columnCount];
			for (int i = 1; i <= rowCount; i++) {
				
				//3. Go To Row
				XSSFRow row=sheet.getRow(i);
				for (int j = 0; j < columnCount; j++) {
					
					//4. Go To Column
					XSSFCell column=row.getCell(j);
					
					//Read Data
					data[i-1][j]=column.getStringCellValue();
					//column.setCellValue(str);
								
				}
								
			}
			
			return data;
			
		}
}
