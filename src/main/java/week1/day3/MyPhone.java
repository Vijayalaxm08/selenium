package week1.day3;

public class MyPhone {

	public static void main(String[] args) {
	
		
		Samsung sam1=new Samsung();
		String mobileColor1 = sam1.mobileColor();
		System.out.println(mobileColor1);
		int mobileSize1 = sam1.mobileSize();
		System.out.println(mobileSize1);
		sam1.modelNumber();
		
		Jio j=new Jio();
		int dialCaller = j.dialCaller();
		System.out.println(dialCaller);
		
		String mobileColor = j.mobileColor();
		System.out.println(mobileColor);
		
		int mobileSize = j.mobileSize();
		System.out.println(mobileSize);
		
		String model = j.model();
		System.out.println(model);
		
		String netConnection = j.netConnection();
		System.out.println(netConnection);
		j.ownJio();
			
		
		MobilePhone sam=new Samsung();
		String mobileColor2 = sam.mobileColor();
		System.out.println(mobileColor2);
		int mobileSize2 = sam.mobileSize();
		System.out.println(mobileSize2);
		
		MobilePhone jMobile=new Jio();
		String mobileColor3 = jMobile.mobileColor();
		System.out.println(mobileColor3);
		int mobileSize3 = jMobile.mobileSize();
		System.out.println(mobileSize3);
		
		Telephone jTelephone=new Jio();
		int dialCaller2 = jTelephone.dialCaller();
		System.out.println(dialCaller2);
		int mobileSize4 = jTelephone.mobileSize();
		System.out.println(mobileSize4);
		String model2 = jTelephone.model();
		System.out.println(model2);
		String netConnection2 = jTelephone.netConnection();
		System.out.println(netConnection2);
		
		//Telephone sam2=new Samsung(); - Error Message since Samsung class did not implement the Telephone Interface
				


	}

}
