package week1.day3;

public interface Telephone {

	public String netConnection();
	public int dialCaller();
	public String model();
	public int mobileSize();	
	
}
