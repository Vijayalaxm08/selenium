package week1.day3;

public class Jio implements MobilePhone, Telephone{

	@Override
	public String netConnection() {
		// TODO Auto-generated method stub
		return "Airtel";
	}

	@Override
	public int dialCaller() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public String model() {
		// TODO Auto-generated method stub
		return "Moto";
	}

	@Override
	public String mobileColor() {
		// TODO Auto-generated method stub
		return "Metallic Grey";
	}

	@Override
	public int mobileSize() {
		// TODO Auto-generated method stub
		return 15;
	}
	
	public void ownJio() {
		System.out.println("Own Jio Method");
	}

}
