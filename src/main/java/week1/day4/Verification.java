package week1.day4;

import org.openqa.selenium.Point;
import org.openqa.selenium.chrome.ChromeDriver;

public class Verification {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		
		//getTitle
		String title = driver.getTitle();
		if(title.equals("Leaftaps - TestLeaf Automation Platform")) {
			System.out.println("Title Displayed Correctly");
		}
		
		//Enter UserName
		driver.findElementById("username").sendKeys("DemoSalesManager");
		//Enter Password
		driver.findElementById("password").sendKeys("crmsfa");
		//Click Login
		driver.findElementByClassName("decorativeSubmit").click();
		
		//getCurrentURL
		String currentUrl = driver.getCurrentUrl();
		if(currentUrl.contains("login")) {
			System.out.println("Page is Logged in");
		}
		
		//getText
		String text = driver.findElementById("appName").getText();
		System.out.println(text);
		
		//Click on Link 
		driver.findElementByLinkText("CRM/SFA").click();
		
		//getAttribute
		String attributeHref = driver.findElementByLinkText("Create Lead").getAttribute("href");
		System.out.println("The Attribute Value is: "+attributeHref);
		
		//Click on Leads
		driver.findElementByLinkText("Leads").click();
		//Click on Create Lead
		driver.findElementByLinkText("Create Lead").click();
				
		//getLocation
		Point location = driver.findElementById("createLeadForm_importantNote").getLocation();
		System.out.println(location);
		
		int x = driver.findElementById("createLeadForm_importantNote").getLocation().getX();
		int y = driver.findElementById("createLeadForm_importantNote").getLocation().getY();
		
		System.out.println(x+" "+y);
		
		
		

	}

}
