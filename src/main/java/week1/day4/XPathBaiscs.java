package week1.day4;

import org.openqa.selenium.chrome.ChromeDriver;

public class XPathBaiscs {

	public static void main(String[] args) throws InterruptedException {
	
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		
		//Enter UserName
		driver.findElementById("username").sendKeys("DemoSalesManager");
		//Enter Password
		driver.findElementById("password").sendKeys("crmsfa");
		//Click Login
		driver.findElementByClassName("decorativeSubmit").click();
		//Click on Link 
		driver.findElementByLinkText("CRM/SFA").click();
		//Click on Leads
		driver.findElementByLinkText("Leads").click();
		Thread.sleep(3000);
		
		
		//First Name X-Path - Collection
// 		(//input[@name='firstName'])[3]
		
		//Last Name X-PAth- Collection
//		(//input[@name='lastName'])[3]
		
		//Find Lead ->Second Calender
//		(//img[contains(@id,'ext-gen')])[6]
		
	}

}
