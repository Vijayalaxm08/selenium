package week1.day2;

import org.openqa.selenium.ie.InternetExplorerDriver;

public class LoginIE {

	public static void main(String[] args) {
	
		System.setProperty("webdriver.ie.driver", "./drivers/IEDriverServer.exe");
		InternetExplorerDriver driver=new InternetExplorerDriver();
		
		//load URL
		driver.get("http://leaftaps.com/opentaps");
		//maximize the browser
		driver.manage().window().maximize();
		

	}

}
