package week1.day2;

import org.openqa.selenium.chrome.ChromeDriver;

public class Login {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		//load URL
		driver.get("http://leaftaps.com/opentaps");
		//maximize the browser
		driver.manage().window().maximize();
		
		//Enter UserName
		driver.findElementById("username").sendKeys("DemoSalesManager");
		//Enter Password
		driver.findElementById("password").sendKeys("crmsfa");
		//Click Login
		driver.findElementByClassName("decorativeSubmit").click();
		
		//Click on Link 
		driver.findElementByLinkText("CRM/SFA").click();
		//Click on Leads
		driver.findElementByLinkText("Leads").click();
		//Click on Create Lead
		driver.findElementByLinkText("Create Lead").click();
		
		//Enter Company Name
		driver.findElementById("createLeadForm_companyName").sendKeys("ABC");
		//Enter First Name
		driver.findElementById("createLeadForm_firstName").sendKeys("Vijayalaxmi");
		//Enter Last Name
		driver.findElementById("createLeadForm_lastName").sendKeys("Krishnarajan");
		//Click Create Lead
		driver.findElementByClassName("smallSubmit").click();
		
		
		//Print the Fist Name and Last Name
		String firstName = driver.findElementById("viewLead_firstName_sp").getText();
		String lastName = driver.findElementById("viewLead_lastName_sp").getText();
		System.out.println(firstName);
		System.out.println(lastName);
		
		//Verify First Name 
		if(firstName.equalsIgnoreCase("vijaYAlaxmi")) {
			System.out.println("First Name is Displayed Correctly");
		}
		else {
			System.out.println("First Name is Not Displayed Correctly");
		}

		//Verify Last Name
		if(lastName.contains("Kris")) {
			System.out.println("Last Name is Displayed Correctly");
		}
		else {
			System.out.println("Last Name is Not Displayed Correctly");
		}
		//Verify First Name using equals
		if(firstName.equals("vijaYAlaxmi")) {
			System.out.println("First Name is Displayed Correctly");
		}
		else {
			System.out.println("First Name is Not Displayed Correctly");
		}
		
	}

}
