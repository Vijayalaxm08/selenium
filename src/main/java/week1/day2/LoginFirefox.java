package week1.day2;

import org.openqa.selenium.firefox.FirefoxDriver;

public class LoginFirefox {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
		FirefoxDriver driver=new FirefoxDriver();
		
		//load URL
		driver.get("http://leaftaps.com/opentaps");
		//maximize the browser
		driver.manage().window().maximize();
	}

}
