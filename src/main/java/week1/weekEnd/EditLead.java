package week1.weekEnd;

import org.openqa.selenium.chrome.ChromeDriver;

public class EditLead {

	public static void main(String[] args) throws InterruptedException {
	
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		//Launch the browser
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		
		//2	Enter the username
		driver.findElementById("username").sendKeys("DemoSalesManager");
		
		//3	Enter the password
		driver.findElementById("password").sendKeys("crmsfa");
		
		//4	Click Login
		driver.findElementByClassName("decorativeSubmit").click();
		
		//5	Click crm/sfa link
		driver.findElementByLinkText("CRM/SFA").click();
		
		//6	Click Leads link
		driver.findElementByLinkText("Leads").click();
		
		//7	Click Find leads
		driver.findElementByLinkText("Find Leads").click();
		
		//8	Enter first name 
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Vijayalaxmi");
		
		//9	Click Find leads button
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		
		Thread.sleep(3000);
		
		//10 Click on first resulting lead
		String firstLeadID = driver.findElementByXPath("(//a[@class='linktext'])[4]").getText();
		System.out.println("The First Resulting Lead ID is: "+ firstLeadID);
		
		driver.findElementByLinkText(firstLeadID).click();
		
		//11	Verify title of the page
		String pageTitle = driver.getTitle();
		if(pageTitle.equals("View Lead | opentaps CRM")) {
			System.out.println("Tile of the Page is :" +pageTitle);
		}
		
		//12	Click Edit
		driver.findElementByLinkText("Edit").click();
		
		//13	Change the company name
		driver.findElementById("updateLeadForm_companyName").clear();
		driver.findElementById("updateLeadForm_companyName").sendKeys("XYZ");
		String companyName = driver.findElementById("updateLeadForm_companyName").getAttribute("value");
		System.out.println("The Newwly Changed Company Name is: "+companyName);
		
		//14	Click Update
		driver.findElementByXPath("(//input[@name='submitButton'])[1]").click();

		//15	Confirm the changed name appears
		String companyNameChanged = driver.findElementById("viewLead_companyName_sp").getText();
		if(companyNameChanged.contains(companyName)) {
			System.out.println("The Changed Company Name is Displayed Correctly");
		}
		
		//16	Close the browser (Do not log out)
		driver.close();
		
	}

}
