package week1.weekEnd;

import org.openqa.selenium.chrome.ChromeDriver;

public class DeleteLead {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		//launch URL
		driver.get("http://leaftaps.com/opentaps");
		//Maximize the Browser window
		driver.manage().window().maximize();
		
		//Enter UserName
		driver.findElementById("username").sendKeys("DemoSalesManager");
		//Enter Password
		driver.findElementById("password").sendKeys("crmsfa");
		//Click Login
		driver.findElementByClassName("decorativeSubmit").click();
		
		
//		Login l=new Login();
//		l.Login();
		
//				
//		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
//		ChromeDriver driver=new ChromeDriver();
		
		//5	Click crm/sfa link
		driver.findElementByLinkText("CRM/SFA").click();
		
		//6	Click Leads link
		driver.findElementByLinkText("Leads").click();
		
		//7	Click Find leads
		driver.findElementByLinkText("Find Leads").click();
		
		//8	Click on Phone
		driver.findElementByXPath("(//span[@class='x-tab-strip-text '])[2]").click();
		
		//9	Enter phone number
		driver.findElementByName("phoneNumber").sendKeys("9003228239");
		
		//10	Click Find leads button
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		
		//11	Capture lead ID of First Resulting lead
		String firstLeadID = driver.findElementByXPath("(//a[@class='linktext'])[4]").getText();
		System.out.println("The First Resulting Lead ID is: "+ firstLeadID);
		
		//12	Click First Resulting lead
		driver.findElementByLinkText(firstLeadID).click();
		
		//13	Click Delete
		driver.findElementByLinkText("Delete").click();
		Thread.sleep(5000);
		
		//14	Click Find leads
		driver.findElementByLinkText("Find Leads").click();
		
		//15	Enter captured lead ID
		driver.findElementByName("id").sendKeys(firstLeadID);
		
		//16	Click find leads button
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		
		//17	Verify error msg
		String errMsg = driver.findElementByClassName("x-paging-info").getText();
		if(errMsg.equals("No records to display")) {
			System.out.println("Error Message is Displayed Correctly");
		}
		
		//18	Close the browser (Do not log out)
		driver.close();
	}

}
