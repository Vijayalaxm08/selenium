package week1.weekEnd;

import org.openqa.selenium.chrome.ChromeDriver;

public class DuplicateLead {

	public static void main(String[] args) throws InterruptedException {
		
	//	Login l=new Login();
		
//		l.Login();
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		//launch URL
		driver.get("http://leaftaps.com/opentaps");
		//Maximize the Browser window
		driver.manage().window().maximize();
		
		//Enter UserName
		driver.findElementById("username").sendKeys("DemoSalesManager");
		//Enter Password
		driver.findElementById("password").sendKeys("crmsfa");
		//Click Login
		driver.findElementByClassName("decorativeSubmit").click();
					
		//5	Click crm/sfa link
		driver.findElementByLinkText("CRM/SFA").click();
		
		//6	Click Leads link
		driver.findElementByLinkText("Leads").click();
		
		//7	Click Find leads
		driver.findElementByLinkText("Find Leads").click();
		
		//8	Click on Email
		driver.findElementByXPath("(//span[@class='x-tab-strip-text '])[3]").click();
		
		//9	Enter Email
		driver.findElementByName("emailAddress").sendKeys("vijayalaxmi.thilaga@gmail.com");
		
		//10	Click find leads button
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);

		//11	Capture name of First Resulting lead
		String firstLeadID = driver.findElementByXPath("(//a[@class='linktext'])[4]").getText();
		System.out.println("The First Resulting Lead ID is: "+ firstLeadID);
		
		//12	Click First Resulting lead
		driver.findElementByLinkText(firstLeadID).click();
		String firstName = driver.findElementById("viewLead_firstName_sp").getText();
		
		//13	Click Duplicate Lead
		driver.findElementByLinkText("Duplicate Lead").click();
		Thread.sleep(5000);
		
		//14	Verify the title as 'Duplicate Lead'
		String title = driver.getTitle();
		if(title.contains("Duplicate Lead")) {
			System.out.println("The Title of the Page " +title + " is displayed Correctly");
		}
		
		//15	Click Create Lead
		driver.findElementByXPath("//input[@value='Create Lead']").click();
		
		//16	Confirm the duplicated lead name is same as captured name
		String firstNameDuplicate = driver.findElementById("viewLead_firstName_sp").getText();
		
		if(firstName.equals(firstNameDuplicate)) {
			System.out.println("The Duplicated First Name is same as the Captured Name");
		}
		
		//17	Close the browser (Do not log out)
		driver.close();
	}

}
