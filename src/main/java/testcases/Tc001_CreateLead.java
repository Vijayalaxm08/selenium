package testcases;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utils.ReadExcelData;
import wdmethods.ProjectSpecificMethods;
import wdmethods.SeMethods;

public class Tc001_CreateLead extends ProjectSpecificMethods{
	
	@BeforeTest
	public void returnData() {
		
		testName="Tc001_CreateLead";
		testDesc="Creating a Lead";
		author="Viji";
		category="Smoke";
	}
	
	@Test(dataProvider="fetchData")
	public void createLead(String compName, String fName, String lName) throws InterruptedException, IOException {
				
		WebElement eleCreateLead = locateElement("link", "Create Lead");
		click(eleCreateLead);
		
		WebElement eleCompName = locateElement("id","createLeadForm_companyName");
		type(eleCompName,compName);
		WebElement eleFirstName = locateElement("id","createLeadForm_firstName");
		type(eleFirstName,fName);
		WebElement eleLastName = locateElement("id","createLeadForm_lastName");
		type(eleLastName,lName);
		
		WebElement eleSouce = locateElement("name","dataSourceId");
		selectDropDownUsingText(eleSouce,"Self Generated");
		WebElement eleCampId = locateElement("id","createLeadForm_marketingCampaignId");
		selectDropDownUsingText(eleCampId,"Automobile");
		WebElement elefNameLocal = locateElement("id","createLeadForm_firstNameLocal");
		type(elefNameLocal,"Vijayalaxmi Local");
		WebElement elelNameLocal = locateElement("id","createLeadForm_lastNameLocal");
		type(elelNameLocal,"Krishnarajan Local");		
		WebElement eleSalutation = locateElement("id","createLeadForm_personalTitle");
		type(eleSalutation,"Ms");		
		WebElement eleTitle = locateElement("id","createLeadForm_generalProfTitle");
		type(eleTitle,"Ms");		
		WebElement eleDeptName = locateElement("id","createLeadForm_departmentName");
		type(eleDeptName,"Automation");		
		WebElement eleAnnualRevenue = locateElement("id","createLeadForm_annualRevenue");
		type(eleAnnualRevenue,"1200000");		
		WebElement eleCurrencyID = locateElement("id","createLeadForm_currencyUomId");
		selectDropDownUsingText(eleCurrencyID, "INR - Indian Rupee");
		WebElement eleEnumId = locateElement("id","createLeadForm_industryEnumId");
		selectDropDownUsingIndex(eleEnumId,3);
		
		WebElement eleNoOfEmployees = locateElement("id","createLeadForm_numberEmployees");
		type(eleNoOfEmployees,"50");
		WebElement eleOwnership = locateElement("id","createLeadForm_ownershipEnumId");
		selectDropDownUsingText(eleOwnership, "Partnership");		
		WebElement eleSICCode = locateElement("id","createLeadForm_sicCode");
		type(eleSICCode,"772577");
		WebElement eleTickerSymbol = locateElement("id","createLeadForm_tickerSymbol");
		type(eleTickerSymbol,"Yes");		
		WebElement eleDescription = locateElement("id","createLeadForm_description");
		type(eleDescription,"Creation of Create Lead");		
		WebElement eleImportantNote = locateElement("id","createLeadForm_importantNote");
		type(eleImportantNote,"Important Note");
		
		//Contact Information
		WebElement eleAreaCode = locateElement("id","createLeadForm_primaryPhoneAreaCode");
		type(eleAreaCode,"91");
		WebElement elePhoneNumber = locateElement("id","createLeadForm_primaryPhoneNumber");
		type(elePhoneNumber,"9003228239");
		WebElement elePhoneExt = locateElement("id","createLeadForm_primaryPhoneExtension");
		type(elePhoneExt,"66016");
		WebElement eleAskForName = locateElement("id","createLeadForm_primaryPhoneAskForName");
		type(eleAskForName,"Krishnarajan");
		WebElement eleEmailId = locateElement("id","createLeadForm_primaryEmail");
		type(eleEmailId,"vijayalaxmi.thilaga@gmail.com");
		WebElement eleWebURL = locateElement("id","createLeadForm_primaryWebUrl");
		type(eleWebURL,"https://www.gmail.com");

		//Primary Address
		WebElement eleToName = locateElement("id","createLeadForm_generalToName");
		type(eleToName,"Viji");
		WebElement eleAttnName = locateElement("id","createLeadForm_generalAttnName");
		type(eleAttnName,"Thilaga");
		WebElement eleAddr1 = locateElement("id","createLeadForm_generalAddress1");
		type(eleAddr1,"AGS Villa");
		WebElement eleAddr2 = locateElement("id","createLeadForm_generalAddress2");
		type(eleAddr2,"Pallikaranai");
		WebElement eleCity = locateElement("id","createLeadForm_generalCity");
		type(eleCity,"Chennai");
		WebElement eleState = locateElement("id","createLeadForm_generalStateProvinceGeoId");
		selectDropDownUsingText(eleState, "Minnesota");	
		WebElement elePostalCode = locateElement("id","createLeadForm_generalPostalCode");
		type(elePostalCode,"600100");
		WebElement eleCountry = locateElement("id","createLeadForm_generalCountryGeoId");
		selectDropDownUsingText(eleCountry, "India");	
		WebElement elel = locateElement("id","createLeadForm_generalPostalCodeExt");
		type(elelNameLocal,"001");
		
		//Click Create Lead
		WebElement eleCreateLeadButton = locateElement("class","smallSubmit");
		eleCreateLeadButton.click();
		
		//Print the Fist Name and Last Name
		WebElement eleViewFName = locateElement("id","viewLead_firstName_sp");
		WebElement eleViewLName = locateElement("id","viewLead_lastName_sp");
			
		verifyExactText(eleViewFName, "vijaYAlaxmi");
		verifyPartialText(eleViewLName, "Kris");
											
	}

	@DataProvider(name="fetchData")
	public String[][] getData() throws IOException {
		
		return ReadExcelData.readExcelData("Tc001_CreateLead");
		
	}
		
}







