package testcases;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utils.ReadExcelData;
import wdmethods.ProjectSpecificMethods;


public class Tc002_EditLead extends ProjectSpecificMethods{
	
	//RemoteWebDriver driver=null;
	@Test(dataProvider="fetchData")
	public void EditLead(String fName, String compName) throws InterruptedException {
		
				
		WebElement eleFindLeads = locateElement("link", "Find Leads");
		click(eleFindLeads);
				
		//8	Enter first name
		WebElement eleFirstName = locateElement("xpath","(//input[@name='firstName'])[3]");
		type(eleFirstName,fName);
		
		//9	Click Find leads button
		WebElement eleFindLeadsButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindLeadsButton);
		
		Thread.sleep(3000);
		//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		//10 Click on first resulting lead
		WebElement firstLeadID = locateElement("xpath", "(//a[@class='linktext'])[4]");
				
		WebElement elefirstLeadID = locateElement("link", getText(firstLeadID));
		click(elefirstLeadID);
		
						
		//11	Verify title of the page
		verifyTitle("View Lead | opentaps CRM");
		
		//12	Click Edit
		WebElement eleEdit = locateElement("link", "Edit");
		click(eleEdit);
					
		//13	Change the company name
		WebElement eleCompanyName = locateElement("id","updateLeadForm_companyName");
		type(eleCompanyName,compName);
		verifyExactAttribute(eleCompanyName,"value",compName);
		String textCompanyName = getText(eleCompanyName);
				
				
		//14	Click Update
		WebElement eleUpdate = locateElement("xpath","(//input[@name='submitButton'])[1]");
		click(eleUpdate);
		
		Thread.sleep(3000);
		//15	Confirm the changed name appears
		WebElement eleCompanyNameChanged = locateElement("id","viewLead_companyName_sp");
		verifyPartialText(eleCompanyNameChanged,textCompanyName);
		
							
	}
	
	@DataProvider(name="fetchData")
	public String[][] getData() throws IOException {
		
		return ReadExcelData.readExcelData("Tc002_EditLead");
		
	}

}
