package testcases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utils.ReadExcelData;
import wdmethods.ProjectSpecificMethods;

public class Tc004_DeleteLead extends ProjectSpecificMethods{

	@Test(dataProvider="fetchData")
	public void DeleteLead(String phoneNum) throws InterruptedException {
		
		WebElement eleFindLeads = locateElement("link", "Find Leads");
		click(eleFindLeads);
				
		//8	Click on Phone
		WebElement elePhone = locateElement("xpath", "(//span[@class='x-tab-strip-text '])[2]");
		click(elePhone);
		
		//9	Enter phone number
		WebElement elePhoneNumber = locateElement("name", "phoneNumber");
		type(elePhoneNumber, phoneNum);
		
		//10	Click Find leads button
		WebElement eleFindLeadsButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindLeadsButton);
		
		Thread.sleep(5000);
							
		//11	Capture lead ID of First Resulting lead
		WebElement eleLeadID = locateElement("xpath", "(//a[@class='linktext'])[4]");
		String firstLeadID = getText(eleLeadID);
		System.out.println("The First Resulting Lead ID is: "+ firstLeadID);
				
		//12	Click First Resulting lead
		WebElement eleFirstResultingLead = locateElement("link", firstLeadID);
		click(eleFirstResultingLead);
		
		//13	Click Delete
		WebElement eleDelete = locateElement("link", "Delete");
		click(eleDelete);
		
		Thread.sleep(5000);
				
		//14	Click Find leads
		WebElement eleFindLeads1= locateElement("link", "Find Leads");
		click(eleFindLeads1);
		
		//15	Enter captured lead ID
		WebElement eleCapturedLeadID = locateElement("name", "id");
		type(eleCapturedLeadID, firstLeadID);
		
		//16	Click find leads button
		WebElement eleFindLeadButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindLeadButton);
		
	    Thread.sleep(3000);
				
		//17	Verify error msg
	    WebElement eleErrMsg = locateElement("class", "x-paging-info");
	    verifyExactText(eleErrMsg, "No records to display");
			
	}
	
	@DataProvider(name="fetchData")
	public String[][] getData() throws IOException {
		
		return ReadExcelData.readExcelData("Tc004_DeleteLead");
		
	}

}
