package week6.day2;

public class UpperLower {

	public static void main(String[] args) {
		
		String name="kowshik";
		
		char[] nameSplit = name.toCharArray();
					
		for (int i = 0; i < nameSplit.length; i++) {
			
			if (i%2==0) {
				
			System.out.print(Character.toLowerCase(nameSplit[i]));	
								
			}else {
				System.out.print(Character.toUpperCase(nameSplit[i]));	
			}
		}
		
	}

}
