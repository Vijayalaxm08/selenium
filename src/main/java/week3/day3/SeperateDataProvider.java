package week3.day3;

import org.testng.annotations.DataProvider;

public class SeperateDataProvider {
	
	@DataProvider(name="fetchDataSeperate")
	public String[][] createData(){
		
		String[][] data=new String[2][7];
		data[0][0]="ABC";
		data[0][1]="Vijayalaxmi";
		data[0][2]="Krishnarajan";
		data[0][3]="Self Generated";
		data[0][4]="Automobile";
		data[0][5]="3";
		data[0][6]="vijayalaxmi.thilaga@gmail.com";
		
		data[1][0]="XYZ";
		data[1][1]="Hariharan";
		data[1][2]="Krishnarajan";
		data[1][3]="Self Generated";
		data[1][4]="Automobile";
		data[1][5]="2";
		data[1][6]="viji.thilaga@gmail.com";
		
		return data;
		
	}

}
