package week3.day3;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

public class SeMethods implements WdMethods {
	
	RemoteWebDriver driver=null;
	@Override
	public void startApp(String browser, String url) {
		// TODO Auto-generated method stub
		if(browser.equalsIgnoreCase("chrome")) {
			System.setProperty("driver.chrome.driver", "./drivers/chromedriver.exe");
			driver=new ChromeDriver();
		}else if(browser.equalsIgnoreCase("firefox")) {
			System.setProperty("driver.gecko.driver", "./drivers/geckodriver.exe");
			driver=new FirefoxDriver();
		}else if(browser.equalsIgnoreCase("ie")) {
			System.setProperty("driver.ie.driver", "./drivers/IEDriverServer.exe");
			driver=new InternetExplorerDriver();
		}		
		driver.get(url);
		driver.manage().window().maximize();
		System.out.println("The Browser " +browser+ " is launched successfully");
}

	@Override
	public WebElement locateElement(String locator, String locValue) {
		
		switch (locator) {
		case "id":
			return driver.findElementById(locValue);
		case "name":
			return driver.findElementByName(locValue);
		case "class":
			return driver.findElementByClassName(locValue);
		case "link":
			return driver.findElementByLinkText(locValue);
		case "tag":
			return driver.findElementByTagName(locValue);
		case "xpath":
			return driver.findElementByXPath(locValue);
		}
		return null;
		
	}

	@Override
	public WebElement locateElement(String locValue) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void type(WebElement ele, String data) {
	   ele.clear();
       ele.sendKeys(data);
       System.out.println("The Data " +data+ " is entered successfully");
	}

	@Override
	public void click(WebElement ele) {
		ele.click();
		System.out.println("The element "+ele+" clicked successfully");
		
	}

	@Override
	public String getText(WebElement ele) {
		String text = ele.getText();
		System.out.println("The Text is Displayed as: " +text);
		return text;		
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		Select s=new Select(ele);
		s.selectByVisibleText(value);		
		System.out.println("The value "+value+" entered successfully");
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		Select s=new Select(ele);
		s.selectByIndex(index);		
	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		String browserTitle = driver.getTitle();
		if(browserTitle.equals(expectedTitle)) {
			System.out.println("The title of the browser is" +browserTitle);
			return true;
			
		}else {
			System.out.println("The title of the browser is" +browserTitle);
			return false;
		}
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		String text = ele.getText();
		if(text.equals(expectedText)) {
			System.out.println("The Text is Displayed Correctly as: " +text);
		}else {
			System.out.println("The Text is Not Displayed Correctly as"+text);
		}	
		
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		String text = ele.getText();
		if(text.contains(expectedText)) {
			System.out.println("The Text is Displayed Correctly as: " +text);
		}else {
			System.out.println("The Text is Not Displayed Correctly as"+text);
		}		
		
	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		String attribute1 = ele.getAttribute(attribute);
		if(attribute1.equalsIgnoreCase(value)) {
			System.out.println("The "+attribute+" has the value "+attribute1);
		}else {
			System.out.println("The attribute is not matched");
		}
		
		
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		String attribute1 = ele.getAttribute(attribute);
		if(attribute1.contains(value)) {
			System.out.println("The "+attribute+" has the value "+attribute1);
		}else {
			System.out.println("The attribute is not matched");
		}
						
	}

	@Override
	public void verifySelected(WebElement ele) {
		if (ele.isSelected()) {
			System.out.println("The Element is Displayed");
		}else {
			System.out.println("The Element is Not Displayed");
		}
		
	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		if (ele.isDisplayed()) {
			System.out.println("The Element is Displayed");
		}else {
			System.out.println("The Element is Not Displayed");
		}
		
	}

	@Override
	public void switchToWindow(int index) {
		
		Set<String> set=driver.getWindowHandles();
		
		List<String> list=new ArrayList<>();
		list.addAll(set);
		driver.switchTo().window(list.get(index));
		System.out.println("The title of Newly Switched Window" +driver.getTitle());
				
	}

	@Override
	public void switchToFrame(WebElement ele) {
		
		driver.switchTo().frame(ele);
		System.out.println("Switched To Frame");
	}

	@Override
	public void acceptAlert() {
		driver.switchTo().alert().accept();
		System.out.println("Alert is accepted");
	}

	@Override
	public void dismissAlert() {
		driver.switchTo().alert().dismiss();
		System.out.println("Alert is accepted");		
	}

	@Override
	public String getAlertText() {
		String alertText = driver.switchTo().alert().getText();
		System.out.println("The Text of the alert is " +alertText);
		return alertText;		
	}

	@Override
	public void takeSnap() throws IOException {
		
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dest=new File("./screenshots/CreateLead.png");
		
		FileUtils.copyFile(src, dest);
		
	}

	@Override
	public void closeBrowser() {
		driver.close();
		System.out.println("The Current Browser is Closed");
		
	}

	@Override
	public void closeAllBrowsers() {
		driver.quit();
		System.out.println("All the Browser is Closed");
		
	}

}
