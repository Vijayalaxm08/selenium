package week3.day3;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class DataProviderExcelSheet {

	public static void main(String[] args) throws IOException {

		//1. Open Workbook
		
		XSSFWorkbook wbook=new XSSFWorkbook("./data/SampleTest.xlsx");
		
		//2. Go To Sheet
		XSSFSheet sheet = wbook.getSheetAt(0);
		
		//3. Go To Row
		XSSFRow row = sheet.getRow(1);
		
		//4. Go To  Column
		XSSFCell column = row.getCell(2);
		
		//5. Read Data
		String cellValue = column.getStringCellValue();
		
		System.out.println("The Cell Value is : "+cellValue);
	}

}
