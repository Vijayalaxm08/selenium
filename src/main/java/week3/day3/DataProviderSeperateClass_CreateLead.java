package week3.day3;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdmethods.ProjectSpecificMethods;
import wdmethods.SeMethods;

public class DataProviderSeperateClass_CreateLead extends ParameterProjectSpecificMethods{

	@Test(dataProvider= "fetchDataSeperate", dataProviderClass=SeperateDataProvider.class)
	
	public void createLead(String compName, String fName, String lName,String sourceId, String campaignID, String enumID, String emailID) throws InterruptedException, IOException {
				
		WebElement eleCreateLead = locateElement("link", "Create Lead");
		click(eleCreateLead);
		
		WebElement eleCompName = locateElement("id","createLeadForm_companyName");
		type(eleCompName,compName);
		WebElement eleFirstName = locateElement("id","createLeadForm_firstName");
		type(eleFirstName,fName);
		WebElement eleLastName = locateElement("id","createLeadForm_lastName");
		type(eleLastName,lName);
		
		WebElement eleSouce = locateElement("name","dataSourceId");
		selectDropDownUsingText(eleSouce,sourceId);
		WebElement eleCampId = locateElement("id","createLeadForm_marketingCampaignId");
		selectDropDownUsingText(eleCampId,campaignID);
		WebElement eleEnumId = locateElement("id","createLeadForm_industryEnumId");
		selectDropDownUsingIndex(eleEnumId,Integer.parseInt(enumID));
		
		WebElement eleEmailId = locateElement("id","createLeadForm_primaryEmail");
		type(eleEmailId,emailID);
						
		//Finding the Count of values in Drop Down and Display the Last Value
		WebElement eleCurrencyId= locateElement("id","createLeadForm_currencyUomId"); 
		Select currency=new Select(eleCurrencyId);
				
		List<WebElement> eachOptions = currency.getOptions();
		int count = eachOptions.size();
		System.out.println("Total Number of Values inside Drop Down is:"+count);
		currency.selectByIndex(count-1); // Selecting Last Option in Drop Down
				
		//Printing All the values in the Currency Drop Down
		for (WebElement eachValue:eachOptions) {
			System.out.println(eachValue.getText());
		}
		
		WebElement eleCreateLead1 = locateElement("class", "smallSubmit");
		click(eleCreateLead1);
		
		takeSnap();
						
	}
		
}







