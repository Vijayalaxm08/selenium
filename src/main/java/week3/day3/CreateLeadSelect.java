package week3.day3;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class CreateLeadSelect {
	@Parameters({"browser", "url", "username", "password"})
	@Test(dataProvider="fetchingData")
	public void createLead(String browser, String url, String uname, String password, String compName, String fName, String lName) throws IOException {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		driver.get(url);
		driver.manage().window().maximize();
		
		//Enter UserName
		driver.findElementById("username").sendKeys(uname);
		//Enter Password
		driver.findElementById("password").sendKeys(password);
		//Click Login
		driver.findElementByClassName("decorativeSubmit").click();
		
		//Click on Link 
		driver.findElementByLinkText("CRM/SFA").click();
		//Click on Leads
		driver.findElementByLinkText("Leads").click();
		//Click on Create Lead
		driver.findElementByLinkText("Create Lead").click();
		
		//Enter Company Name
		driver.findElementById("createLeadForm_companyName").sendKeys(compName);
		//Enter First Name
		driver.findElementById("createLeadForm_firstName").sendKeys(fName);
		//Enter Last Name
		driver.findElementById("createLeadForm_lastName").sendKeys(lName);	
		
		//Selecting the Value from Drop Down using Visible Text
		WebElement elementSrc = driver.findElementById("createLeadForm_dataSourceId");
		Select sc=new Select(elementSrc);
		sc.selectByVisibleText("Self Generated");
		
		List<WebElement> allSource=sc.getOptions();
		System.out.println("Total Number of Values inside Source Drop Down is:"+allSource.size());
		for(WebElement eachSource:allSource) {
			System.out.println(eachSource.getText());
		}
		
		//Selecting the Value from Drop Down using Value
		WebElement elementMarketing = driver.findElementById("createLeadForm_marketingCampaignId");
		Select campId=new Select(elementMarketing);
		campId.selectByValue("DEMO_MKTG_CAMP");
		
		//Selecting the Value from Drop Down using Index
		WebElement industry=driver.findElementById("createLeadForm_industryEnumId");
		Select ind=new Select(industry);
		ind.selectByIndex(3);
		
		//Finding the Count of values in Drop Down and Display the Last Value
		WebElement prefCurr=driver.findElementById("createLeadForm_currencyUomId");
		Select currency=new Select(prefCurr);
		
		List<WebElement> eachOptions = currency.getOptions();
		int count = eachOptions.size();
		System.out.println("Total Number of Values inside Drop Down is:"+count);
		currency.selectByIndex(count-1); // Selecting Last Option in Drop Down
		
		//Printing All the values in the Currency Drop Down
		for (WebElement eachValue:eachOptions) {
			System.out.println(eachValue.getText());
		}
		
		//Click Create Lead
		driver.findElementByClassName("smallSubmit").click();
		
		//Taking Screenshot
		File source = driver.getScreenshotAs(OutputType.FILE);
		File dest=new File("./screenshots/CreateLead.png");
		
		FileUtils.copyFile(source, dest);
			
		
	}

@DataProvider(name="fetchingData")
public String[][] getData() {
		
		String[][] data=new String[2][3];
		data[0][0]="ABC";
		data[0][1]="Vijayalaxmi";
		data[0][2]="Krishnarajan";
		
		data[1][0]="XYZ";
		data[1][1]="Hariharan";
		data[1][2]="Krishnarajan";
		
		return data;
		
	}

}
