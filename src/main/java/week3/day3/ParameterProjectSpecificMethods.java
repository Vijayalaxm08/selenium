package week3.day3;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

public class ParameterProjectSpecificMethods extends SeMethods {
    @Parameters({"browser", "url", "username", "password"})
	@BeforeMethod(groups="Common")
	public void login(String browser, String URL, String uname, String password) {
		
		startApp(browser, URL);
		WebElement eleID = locateElement("id","username");
		type(eleID,uname);
		WebElement elePassword = locateElement("id","password");
		type(elePassword,password);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		
		WebElement eleCrm = locateElement("link", "CRM/SFA");
		click(eleCrm);
		WebElement eleLeads = locateElement("link", "Leads");
		click(eleLeads);
	}
	
	@AfterMethod(groups="Common")
	public void closeBrowser() {
		closeAllBrowsers();
	}
}
