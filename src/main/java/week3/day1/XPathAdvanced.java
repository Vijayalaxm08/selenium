package week3.day1;

import java.util.List;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class XPathAdvanced {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		//Launch the browser
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		
		//2	Enter the username
		driver.findElementById("username").sendKeys("DemoSalesManager");
		
		//3	Enter the password
		driver.findElementById("password").sendKeys("crmsfa");
		
		//4	Click Login
		driver.findElementByClassName("decorativeSubmit").click();
		
		//5	Click crm/sfa link
		driver.findElementByLinkText("CRM/SFA").click();
		
		//6	Click Leads link
		driver.findElementByLinkText("Leads").click();
		
		//7	Click Find leads
		driver.findElementByLinkText("Find Leads").click();
		
		//8	Enter first name 
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Vijayalaxmi");
		
		//9	Click Find leads button
		driver.findElementByXPath("//button[text()='Find Leads']").click();
				
		Thread.sleep(3000);
		
		//Based on the Phone Number, fine the respective lead Id
		String leadID = driver.findElementByXPath("(//div[contains(text(),'9003228239')])[7]/preceding::a[@class='linktext'][5]").getText();
		System.out.println("The Lead ID for the number is :" +leadID);
		
		
		//Based on the Company Name, list all the Lead id

		List<WebElement> list=driver.findElementsByXPath("//div[contains(@class,'col-companyName')]");
		List<WebElement> list1=driver.findElementsByXPath("//div[contains(@class,'col-partyId')]");
		
		System.out.println(list.size());
		for (int i = 1; i < list.size(); i++) {
		
			if(list.get(i).getText().equals("ABC")) {
				
				System.out.println(list1.get(i).getText());
				
				
			}
			
		}

		
		

	}

}
