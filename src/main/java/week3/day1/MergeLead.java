package week3.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class MergeLead {
	
	@Test(invocationCount=1, invocationTimeOut=20000)
	public void MergeLeadInvocationTimeOut() throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		//Launch the browser
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		
		//2	Enter the username
		driver.findElementById("username").sendKeys("DemoSalesManager");
		
		//3	Enter the password
		driver.findElementById("password").sendKeys("crmsfa");
		
		//4	Click Login
		driver.findElementByClassName("decorativeSubmit").click();
		
		//5	Click crm/sfa link
		driver.findElementByLinkText("CRM/SFA").click();
		
		//6	Click Leads link
		driver.findElementByLinkText("Leads").click();
		
		//7	Click Merge leads
		driver.findElementByLinkText("Merge Leads").click();
		
		//8	Click on Icon near From Lead
		driver.findElementByXPath("(//img[contains(@src,'fieldlookup')])[1]").click();
		Thread.sleep(4000);
		
		//9	Move to new window
		Set<String> allWindow = driver.getWindowHandles();
		
		//Move to List
		List<String> list=new ArrayList<String>();
		list.addAll(allWindow);
		
		System.out.println("The Size of the List is " +list.size());
		driver.switchTo().window(list.get(1));
		System.out.println("The Title of New Window is: "+driver.getTitle());
		driver.manage().window().maximize();
		
		//10	Enter First Name
		driver.findElementByName("firstName").sendKeys("Vijayalaxmi");
		
		//11	Click Find Leads button
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(5000);
		
		//12	Click First Resulting lead
		String firstLead = driver.findElementByXPath("(//a[@class='linktext'])[1]").getText();
		System.out.println("The First Resulting LEad is : "+firstLead);
		driver.findElementByLinkText(firstLead).click();
		
		//13	Switch back to primary window
		driver.switchTo().window(list.get(0));
		
		//14	Click on Icon near To Lead
		driver.findElementByXPath("(//img[contains(@src,'fieldlookup')])[2]").click();
		Thread.sleep(4000);
		
		//15	Move to new window
		allWindow = driver.getWindowHandles();
		//Move to List
		List<String> list1=new ArrayList<String>();
		list1.addAll(allWindow);
		
		System.out.println("The Size of the Window is " +list1.size());
		driver.switchTo().window(list1.get(1));
		System.out.println("The Title of New Window is: "+driver.getTitle());
		driver.manage().window().maximize();
		
		//16	Enter First Name
		driver.findElementByName("firstName").sendKeys("Vijayalaxmi");
		
		//17	Click Find Leads button
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		
        //18	Click First Resulting lead
		String firstLead1 = driver.findElementByXPath("(//a[@class='linktext'])[7]").getText();
		System.out.println("The First Resulting LEad is : "+firstLead1);
		driver.findElementByLinkText(firstLead1).click();
		
		//19	Switch back to primary window
		driver.switchTo().window(list1.get(0));
					
		//20	Click Merge
		driver.findElementByXPath("//a[text()='Merge']").click();
		
		//21	Accept Alert
		driver.switchTo().alert().accept();
		Thread.sleep(3000);
		
		//22	Click Find Leads
		driver.findElementByLinkText("Find Leads").click();
		Thread.sleep(4000);
		
		//23	Enter From Lead ID
		driver.findElementByXPath("//input[@name='id']").sendKeys(firstLead);
		
		//24	Click Find Leads
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		
		//25	Verify error msg
		String errMsg = driver.findElementByClassName("x-paging-info").getText();
		if(errMsg.equals("No records to display")) {
			System.out.println("Error Message is Displayed Correctly");
		}
		//26	Close the browser (Do not log out)
		driver.close();

	}

}
