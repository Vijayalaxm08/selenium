package week3.day1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdmethods.SeMethods;

public class DependsOnMethod extends SeMethods{
	
	@Test(dependsOnMethods="testcases.Tc001_CreateLead.login",alwaysRun=true)
	public void EditLead() throws InterruptedException {
		startApp("chrome","http://leaftaps.com/opentaps");
		WebElement eleID = locateElement("id","username");
		type(eleID,"DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword,"crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		
		WebElement eleCrm = locateElement("link", "CRM/SFA");
		click(eleCrm);
		WebElement eleLeads = locateElement("link", "Leads");
		click(eleLeads);
		
		WebElement eleFindLeads = locateElement("link", "Find Leads");
		click(eleFindLeads);
		
		
		//8	Enter first name
		WebElement eleFirstName = locateElement("xpath","(//input[@name='firstName'])[3]");
		type(eleFirstName,"Vijayalaxmi");
		
		//9	Click Find leads button
		WebElement eleFindLeadsButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindLeadsButton);
		
		Thread.sleep(3000);
		//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		//10 Click on first resulting lead
		WebElement firstLeadID = locateElement("xpath", "(//a[@class='linktext'])[4]");
				
		WebElement elefirstLeadID = locateElement("link", getText(firstLeadID));
		click(elefirstLeadID);
		
						
		//11	Verify title of the page
		verifyTitle("View Lead | opentaps CRM");
		
		//12	Click Edit
		WebElement eleEdit = locateElement("link", "Edit");
		click(eleEdit);
					
		//13	Change the company name
		WebElement eleCompanyName = locateElement("id","updateLeadForm_companyName");
		type(eleCompanyName,"XYZ");
		verifyExactAttribute(eleCompanyName,"value","XYZ");
		String textCompanyName = getText(eleCompanyName);
				
				
		//14	Click Update
		WebElement eleUpdate = locateElement("xpath","(//input[@name='submitButton'])[1]");
		click(eleUpdate);
		
		Thread.sleep(3000);
		//15	Confirm the changed name appears
		WebElement eleCompanyNameChanged = locateElement("id","viewLead_companyName_sp");
		verifyPartialText(eleCompanyNameChanged,textCompanyName);
		
		/*String companyNameChanged = driver.findElementById("viewLead_companyName_sp").getText();
		if(companyNameChanged.contains(companyName)) {
			System.out.println("The Changed Company Name is Displayed Correctly");
		}
			*/	
		//16	Close the browser (Do not log out)
		closeBrowser();
	}
	
	@Test(dependsOnMethods="testcases.Tc001_CreateLead.login")
	public void duplicate() throws InterruptedException {
		startApp("chrome","http://leaftaps.com/opentaps");
		WebElement eleID = locateElement("id","username");
		type(eleID,"DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword,"crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		
		WebElement eleCrm = locateElement("link", "CRM/SFA");
		click(eleCrm);
		WebElement eleLeads = locateElement("link", "Leads");
		click(eleLeads);
		
		WebElement eleFindLeads = locateElement("link", "Find Leads");
		click(eleFindLeads);
				
		//8	Click on Email
		WebElement eleEmail = locateElement("xpath", "(//span[@class='x-tab-strip-text '])[3]");
		click(eleEmail);				
				
		//9	Enter Email
		WebElement eleEmailId = locateElement("name","emailAddress");
		type(eleEmailId,"vijayalaxmi.thilaga@gmail.com");
		
		//10	Click find leads button
		WebElement eleFindLeadsButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindLeadsButton);
		
		Thread.sleep(3000);
		
		//11	Capture name of First Resulting lead
		WebElement firstLeadID = locateElement("xpath", "(//a[@class='linktext'])[4]");
		String textfirstLeadID = getText(firstLeadID);
		
		//12	Click First Resulting lead
		WebElement firstLeadID1 = locateElement("link", textfirstLeadID);
		click(firstLeadID1);
		WebElement viewFirstName = locateElement("id", "viewLead_firstName_sp");
		String firstName = getText(viewFirstName);
				
		//13	Click Duplicate Lead
		WebElement duplicateLead = locateElement("link", "Duplicate Lead");
		click(duplicateLead);
		Thread.sleep(5000);
				
		//14	Verify the title as 'Duplicate Lead'
		verifyTitle("Duplicate Lead");
				
		//15	Click Create Lead
		WebElement createLead = locateElement("xpath", "//input[@value='Create Lead']"); 
		click(createLead);
		
		//16	Confirm the duplicated lead name is same as captured name
		WebElement firstNameDuplicate = locateElement("id", "viewLead_firstName_sp");
		verifyExactText(firstNameDuplicate,firstName);
				
		//17	Close the browser (Do not log out)
		closeAllBrowsers();
	}

}
