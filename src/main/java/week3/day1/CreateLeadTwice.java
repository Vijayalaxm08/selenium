package week3.day1;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import wdmethods.SeMethods;

public class CreateLeadTwice extends SeMethods{

	@Test(invocationCount=2, threadPoolSize=2)
	public void login() throws InterruptedException, IOException {
		
		startApp("chrome","http://leaftaps.com/opentaps");
		WebElement eleID = locateElement("id","username");
		type(eleID,"DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword,"crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		
		Thread.sleep(3000);
		WebElement eleCrm = locateElement("link", "CRM/SFA");
		click(eleCrm);
		WebElement eleLeads = locateElement("link", "Leads");
		click(eleLeads);
		WebElement eleCreateLead = locateElement("link", "Create Lead");
		click(eleCreateLead);
		
		WebElement eleCompName = locateElement("id","createLeadForm_companyName");
		type(eleCompName,"ABC");
		WebElement eleFirstName = locateElement("id","createLeadForm_firstName");
		type(eleFirstName,"Vijayalaxmi");
		WebElement eleLastName = locateElement("id","createLeadForm_lastName");
		type(eleLastName,"Krishnarajan");
		
		WebElement eleSouce = locateElement("name","dataSourceId");
		selectDropDownUsingText(eleSouce,"Self Generated");
		WebElement eleCampId = locateElement("id","createLeadForm_marketingCampaignId");
		selectDropDownUsingText(eleCampId,"Automobile");
		WebElement eleEnumId = locateElement("id","createLeadForm_industryEnumId");
		selectDropDownUsingIndex(eleEnumId,3);
		
		WebElement eleEmailId = locateElement("id","createLeadForm_primaryEmail");
		type(eleEmailId,"vijayalaxmi.thilaga@gmail.com");
						
		//Finding the Count of values in Drop Down and Display the Last Value
		WebElement eleCurrencyId= locateElement("id","createLeadForm_currencyUomId"); 
		Select currency=new Select(eleCurrencyId);
				
		List<WebElement> eachOptions = currency.getOptions();
		int count = eachOptions.size();
		System.out.println("Total Number of Values inside Drop Down is:"+count);
		currency.selectByIndex(count-1); // Selecting Last Option in Drop Down
				
		//Printing All the values in the Currency Drop Down
		for (WebElement eachValue:eachOptions) {
			System.out.println(eachValue.getText());
		}
		
		WebElement eleCreateLead1 = locateElement("class", "smallSubmit");
		click(eleCreateLead1);
		
		takeSnap();
		
		closeAllBrowsers();
				
	}
	
}







