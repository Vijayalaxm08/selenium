package week3.day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdmethods.ProjectSpecificMethods;
import wdmethods.SeMethods;

public class Tc003_DuplicateLead extends ProjectSpecificMethods{

	@Test(groups="Reg",dependsOnGroups="Smoke")
	public void DuplicateLead() throws InterruptedException {
						
		WebElement eleFindLeads = locateElement("link", "Find Leads");
		click(eleFindLeads);
				
		//8	Click on Email
		WebElement eleEmail = locateElement("xpath", "(//span[@class='x-tab-strip-text '])[3]");
		click(eleEmail);				
				
		//9	Enter Email
		WebElement eleEmailId = locateElement("name","emailAddress");
		type(eleEmailId,"vijayalaxmi.thilaga@gmail.com");
		
		//10	Click find leads button
		WebElement eleFindLeadsButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindLeadsButton);
		
		Thread.sleep(3000);
		
		//11	Capture name of First Resulting lead
		WebElement firstLeadID = locateElement("xpath", "(//a[@class='linktext'])[4]");
		String textfirstLeadID = getText(firstLeadID);
		
		//12	Click First Resulting lead
		WebElement firstLeadID1 = locateElement("link", textfirstLeadID);
		click(firstLeadID1);
		WebElement viewFirstName = locateElement("id", "viewLead_firstName_sp");
		String firstName = getText(viewFirstName);
				
		//13	Click Duplicate Lead
		WebElement duplicateLead = locateElement("link", "Duplicate Lead");
		click(duplicateLead);
		Thread.sleep(5000);
				
		//14	Verify the title as 'Duplicate Lead'
		verifyTitle("Duplicate Lead");
				
		//15	Click Create Lead
		WebElement createLead = locateElement("xpath", "//input[@value='Create Lead']"); 
		click(createLead);
		
		//16	Confirm the duplicated lead name is same as captured name
		WebElement firstNameDuplicate = locateElement("id", "viewLead_firstName_sp");
		verifyExactText(firstNameDuplicate,firstName);
		
			
	}
}
